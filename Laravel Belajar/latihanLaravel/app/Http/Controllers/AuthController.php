<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('halaman.daftar');
    }

    public function kirim(Request $request)
    {
        $namaDepan =  $request -> input('fname');
        $namaBelakang =  $request -> input('lname');

        return view('halaman.home', ['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
    }
}
