<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cast;

class CastController extends Controller
{
    public function create()
    {
        return view('cast.tambah');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        //validasi
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required|max:255',
        ]);

        //tambah data
        Cast::create([
            'nama' => $request->input('nama'),
            'umur' => $request->input('umur'),
            'bio' => $request->input('bio'),
        ]);

        //redirect halaman
        return redirect('/cast');
    }

    public function index()
    {
        $cast = Cast::all();
        // dd($cast);
        return view('cast.tampil', ['cast' => $cast]);
    }

    public function show($id)
    {
        $cast = Cast::find($id);
        return view('cast.detail', ['cast' => $cast]);
    }

    public function edit($id)
    {
        $cast = Cast::find($id);
        return view('cast.edit', ['cast' => $cast]);
    }

    public function update($id, Request $request)
    {
        //Validasi
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required|max:255',
        ]);

        $cast = Cast::find($id);
        $cast -> nama = $request -> input('nama');
        $cast -> umur = $request -> input('umur');
        $cast -> bio = $request -> input('bio');
        $cast -> save();

        return redirect('/cast');
    }

    public function destroy($id)
    {
        $cast = Cast::find($id);
        $cast -> delete();

        return redirect('/cast');
    }
}
