<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comments;
use Illuminate\Support\Facades\Auth;

class CommentsController extends Controller
{
    public function store(Request $request, $film_id)
    {
        //Validasi
        $request->validate([
            'content' => 'required',
            'point' => 'required',
        ]);

        $idUser = Auth::id();

        //tambah data
        Comments::create([
            'user_id' => $idUser,
            'film_id' => $film_id,
            'content' => $request->input('content'),
            'point' => $request->input('point'),
        ]);

        return redirect('/film/'. $film_id);
    }
}
