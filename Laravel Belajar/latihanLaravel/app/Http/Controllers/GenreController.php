<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Genre;

class GenreController extends Controller
{
    public function create()
    {
        return view('genre.tambah');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        //validasi
        $request->validate([
            'nama' => 'required|max:255',
        ]);

        //tambah data
        Genre::create([
            'nama' => $request->input('nama'),
        ]);

        //redirect halaman
        return redirect('/genre');
    }

    public function index()
    {
        $genre = Genre::all();
        // dd($cast);
        return view('genre.tampil', ['genre' => $genre]);
    }

    public function show($id)
    {
        $genre = Genre::find($id);
        return view('genre.detail', ['genre' => $genre]);
    }

    public function edit($id)
    {
        $genre = Genre::find($id);
        return view('genre.edit', ['genre' => $genre]);
    }

    public function update($id, Request $request)
    {
        //Validasi
        $request->validate([
            'nama' => 'required|max:255',
        ]);

        $genre = Genre::find($id);
        $genre -> nama = $request -> input('nama');
        $genre -> save();

        return redirect('/genre');
    }

    public function destroy($id)
    {
        $genre = Genre::find($id);
        $genre -> delete();

        return redirect('/genre');
    }
}