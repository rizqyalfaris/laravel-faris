<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    use HasFactory;

    protected $table = "film";

    protected $fillable = ["judul", "ringkasan", "tahun", "poster"];

    public function genre()
    {
        return $this->belongsTo(Genre::class, 'genre_id');
    }

    public function comments()
    {
        return $this->hasMany(Comments::class, 'film_id');
    }
}
