@extends('layouts.master')
@section('judul')
Halaman Edit Cast
@endsection
@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
  @csrf
  @method('put')
  <div class="form-group">
    <label>Nama Pemain</label>
    <input type="text" class="form-control @error('nama') is-invalid @enderror" value="{{$cast->nama}}" name="nama">
  </div>
  @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Umur</label>
    <input type="text" class="form-control @error('umur') is-invalid @enderror" value="{{$cast->umur}}" name="umur">
  </div>
  @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Bio</label>
    <textarea name="bio" cols="30" rows="10" class="form-control @error('bio') is-invalid @enderror">{{$cast->bio}}</textarea>
  </div>
  @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form> 
@endsection