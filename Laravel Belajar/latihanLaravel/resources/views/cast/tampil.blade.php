@extends('layouts.master')
@section('judul')
Halaman Tampil Cast
@endsection
@section('content')
<a href="/cast/create" class="btn btn-primary my-3">Tambah</a>

<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Name</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($cast as $key => $item)
        <tr>
            <th scope="row">{{$key + 1}}</th>
            <td>{{$item->nama}}</td>
            <td>
              <form action="/cast/{{$item->id}}" method="POST">
                @csrf
                @method('delete')
                <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Update</a>
                <input type="submit" value="Delete" onclick="return confirm('Apakah anda yakin ingin menghapus data ini?')" class="btn btn-danger btn-sm">
              </form>
            </td>
        </tr>
    @empty
        <h1>Tidak ada Cast di halaman ini</h1>
    @endforelse
  </tbody>
</table>
@endsection