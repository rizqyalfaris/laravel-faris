@extends('layouts.master')
@section('judul')
Halaman Tambah Film
@endsection
@section('content')
<form action="/film" method="post" enctype="multipart/form-data">
  @csrf
  <div class="form-group">
    <label>Judul Film</label>
    <input type="text" class="form-control @error('judul') is-invalid @enderror" name="judul">
  </div>
  @error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Ringkasan</label>
    <textarea name="ringkasan" cols="30" rows="10" class="form-control @error('ringkasan') is-invalid @enderror"></textarea>
  </div>
  @error('ringkasan')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Tahun</label>
    <input type="number" class="form-control @error('tahun') is-invalid @enderror" name="tahun">
  </div>
  @error('tahun')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Poster</label>
    <input type="file" class="form-control @error('poster') is-invalid @enderror" name="poster">
  </div>
  @error('poster')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Genre</label>
    <select name="genre_id" class="form-control" id="">
        <option value="">--Pilih Genre--</option>
        @forelse ($genre as $item)
            <option value="{{$item->id}}">{{$item->nama}}</option>
        @empty
            <option value="">Tidak ada Data Genre</option>
        @endforelse
    </select>
  </div>
  @error('poster')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection