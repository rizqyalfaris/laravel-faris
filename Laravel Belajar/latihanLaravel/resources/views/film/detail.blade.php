@extends('layouts.master')
@section('judul')
Halaman Detail Film
@endsection
@section('content')
<img src="{{asset('image/'. $film->poster)}}" class="card-img-top" alt="...">
<h3>{{$film->judul}}</h3>
<p class="card-text">{{$film->ringkasan}}</p>

<hr>

<h3>List Kritik</h3>
@forelse ($film -> comments as $item)
<div class="card">
    <div class="card-header">
    {{$item->user->name}}
    </div>
    <div class="card-body">
        <p>Kritik : {{$item->content}}</p>
        <p>Point : {{$item->point}}</p>
    </div>
</div>
@empty
<h5>Tidak ada kritik di Film ini</h5>
@endforelse
<hr>

<form action="/comments/{{$film->id}}" method="post">
    @csrf
    <div class="form-group">
        <label>Content</label>
        <textarea name="content" cols="30" rows="10" placeholder="Isi Kritik" class="form-control @error('content') is-invalid @enderror" name="content"></textarea>
    </div>
    @error('content')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Point</label>
        <input type="text" class="form-control @error('point') is-invalid @enderror" name="point">
    </div>
    @error('point')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

<hr>

<a href="/film" class="btn btn-secondary btn-block btn-sm">Kembali</a>
@endsection