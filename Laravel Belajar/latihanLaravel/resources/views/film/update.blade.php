@extends('layouts.master')
@section('judul')
Halaman Tambah Film
@endsection
@section('content')
<form action="/film/{{$film->id}}" method="post" enctype="multipart/form-data">
  @csrf
  @method('put')
  <div class="form-group">
    <label>Judul Film</label>
    <input type="text" value="{{$film->judul}}" class="form-control @error('judul') is-invalid @enderror" name="judul">
  </div>
  @error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Ringkasan</label>
    <textarea name="ringkasan" cols="30" rows="10" class="form-control @error('ringkasan') is-invalid @enderror">{{$film->ringkasan}}</textarea>
  </div>
  @error('ringkasan')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Tahun</label>
    <input type="number" value="{{$film->tahun}}" class="form-control @error('tahun') is-invalid @enderror" name="tahun">
  </div>
  @error('tahun')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Poster</label>
    <input type="file" class="form-control @error('poster') is-invalid @enderror" name="poster">
  </div>
  @error('poster')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Genre</label>
    <select name="genre_id" class="form-control" id="">
        <option value="">--Pilih Genre--</option>
        @forelse ($genre as $item)
            @if ($item->id === $film->genre_id)
            <option value="{{$item->id}}" selected>{{$item->nama}}</option>
            @else
            <option value="{{$item->id}}">{{$item->nama}}</option>
            @endif
        @empty
            <option value="">Tidak ada Data Genre</option>
        @endforelse
    </select>
  </div>
  @error('poster')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection