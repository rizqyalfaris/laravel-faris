@extends('layouts.master')
@section('judul')
Halaman Detail Genre
@endsection
@section('content')

<h1>{{$genre->nama}}</h1>

<div class="row">
    @forelse ($genre->film as $item)
    <div class="col-4">
    <div class="card">
            <img src="{{asset('image/'. $item->poster)}}" class="card-img-top" alt="...">
            <div class="card-body">
                <h3>{{$item->judul}}</h3>
                <p class="card-text">{{Str::limit($item->ringkasan, 100)}}</p>
                <a href="/film/{{$item->id}}" class="btn btn-info btn-block btn-sm">Detail</a>
            </div>
        </div>
    </div>
    
    @empty
        <h4>Tidak ada Film di kategori ini</h4>
    @endforelse
</div>

@endsection