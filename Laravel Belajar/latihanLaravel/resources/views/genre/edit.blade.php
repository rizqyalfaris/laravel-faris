@extends('layouts.master')
@section('judul')
Halaman Edit Genre
@endsection
@section('content')
<form action="/genre/{{$genre->id}}" method="POST">
  @csrf
  @method('put')
  <div class="form-group">
    <label>Nama Genre</label>
    <input type="text" class="form-control @error('nama') is-invalid @enderror" value="{{$genre->nama}}" name="nama">
  </div>
  @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form> 
@endsection