@extends('layouts.master')
@section('judul')
Halaman Tambah Genre
@endsection
@section('content')
<form action="/genre" method="post">
  @csrf
  <div class="form-group">
    <label>Nama Genre</label>
    <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama">
  </div>
  @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection