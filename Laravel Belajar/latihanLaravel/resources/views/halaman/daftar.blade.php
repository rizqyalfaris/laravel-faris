@extends('layouts.master')
@section('judul')
Halaman Biodata
@endsection
@section('content')
<h3>Sign Up Form</h3>
<form action="/home" method="POST">
    @csrf
    <label>First name:</label><br><br>
    <input type="text" name="fname"><br><br>
    <label>Last name:</label><br><br>
    <input type="text" name="lname"><br><br>
    <label>Gender:</label><br><br>
    <input type="radio" name="gender">Male<br>
    <input type="radio" name="gender">Female<br>
    <input type="radio" name="gender">Other<br><br>
    <label>Nationality:</label>
    <select name="nationality">
        <option value="indonesian">Indonesian</option>
        <option value="singapore">Singapore</option>
        <option value="malaysia">Malaysia</option>
        <option value="thailand">Thailand</option>
        <option value="filipina">Filipina</option>
    </select><br><br>
    <label>Language Spoken:</label><br><br>
    <input type="checkbox">Bahasa Indonesia<br>
    <input type="checkbox">English<br>
    <input type="checkbox">Other<br><br>
    <label>Bio:</label><br><br>
    <textarea name="message" cols="30" rows="10"></textarea>
    <br>
    <input type="submit" value="Sign Up">
</form>
@endsection
