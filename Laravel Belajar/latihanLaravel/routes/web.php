<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\CommentsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [IndexController::class, 'utama']);
Route::get('/register', [AuthController::class, 'register']);
Route::post('/home', [AuthController::class, 'kirim']);
Route::get('/data-tables', [IndexController::class, 'table']);

//<=====================================FILM===============================================>
//CRUD Film
Route::resource('film', FilmController::class);
Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    //<=====================================CAST===============================================>
    //CRUD Cast
    //C -> Create Data
    Route::get('/cast/create', [CastController::class, 'create']);

    //Route untuk menyimpan data ke database
    Route::post('/cast', [CastController::class, 'store']);

    //R -> Read Data
    //Route untuk menampilkan data
    Route::get('/cast', [CastController::class, 'index']);
    //Ambil detail data
    Route::get('/cast/{id}', [CastController::class, 'show']);

    //U -> Update Data
    //Route untuk mengarah ke form data berdasarkan params
    Route::get('/cast/{id}/edit', [CastController::class, 'edit']);
    //Route untuk update data di db berdasarkan params id
    Route::put('/cast/{id}', [CastController::class, 'update']);

    //D -> Delete Data
    //Route untuk delete berdasarkan params id
    Route::delete('/cast/{id}', [CastController::class, 'destroy']);

    //<=====================================GENRE===============================================>
    //CRUD Genre
    //C -> Create Data
    Route::get('/genre/create', [GenreController::class, 'create']);

    //Route untuk menyimpan data ke database
    Route::post('/genre', [GenreController::class, 'store']);

    //R -> Read Data
    //Route untuk menampilkan data
    Route::get('/genre', [GenreController::class, 'index']);
    //Ambil detail data
    Route::get('/genre/{id}', [GenreController::class, 'show']);

    //U -> Update Data
    //Route untuk mengarah ke form data berdasarkan params
    Route::get('/genre/{id}/edit', [GenreController::class, 'edit']);
    //Route untuk update data di db berdasarkan params id
    Route::put('/genre/{id}', [GenreController::class, 'update']);

    //D -> Delete Data
    //Route untuk delete berdasarkan params id
    Route::delete('/genre/{id}', [GenreController::class, 'destroy']);

    Route::post('/comments/{film_id}', [CommentsController::class, 'store']);
});
